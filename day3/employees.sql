-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 31, 2023 at 02:45 PM
-- Server version: 8.0.32-0ubuntu0.20.04.2
-- PHP Version: 8.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ecom`
--

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `id` int UNSIGNED NOT NULL,
  `name` varchar(30) NOT NULL,
  `contact_number` varchar(13) DEFAULT NULL,
  `address` varchar(50) DEFAULT NULL,
  `salary` int DEFAULT NULL,
  `employee_id` int DEFAULT NULL,
  `role` varchar(100) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`id`, `name`, `contact_number`, `address`, `salary`, `employee_id`, `role`, `created_at`, `updated_at`) VALUES
(294, 'Monisha', '8452163798', 'Jaipur', 45880, 54, 'HR', '2022-11-18 19:25:58', '2022-11-18 19:25:58'),
(296, 'prashant', '78676786', 'usa', 87868, 4, 'Devloper', '2022-11-18 19:25:58', '2022-11-18 19:25:58'),
(297, 'shivam', '9842153576', 'mumbai', 90000, 5, 'devloper', '2022-11-18 19:25:58', '2022-11-18 19:25:58'),
(298, 'ishan', '94589854', 'nashik', 95000, 19, 'devloper', '2022-11-18 19:25:58', '2022-11-18 19:25:58'),
(302, 'vedanta', '94589854', 'nashik', 95000, 19, 'devloper', '2022-11-18 19:25:58', '2022-11-18 19:25:58'),
(303, 'Suresh', '8415263797', 'Nagpur', 80000, 121, 'Devloper', '2022-11-18 19:25:58', '2022-11-18 19:25:58'),
(307, 'Rakesh', '754824561', 'Kolhapur', 45100, 45, 'Devloper', '2022-11-18 19:25:58', '2022-11-18 19:25:58'),
(310, 'Vijay', '8891245462', 'Pune', 45200, 22, 'HR', '2022-11-19 02:06:59', '2022-11-19 02:06:59'),
(314, 'Humza', '9548648548', 'Dubai', 95000, 54, 'Manager', '2022-11-28 12:37:42', '2022-11-28 12:37:42'),
(327, 'Hinal', '65487438', 'surat', 950000, NULL, 'CEO', '2022-12-12 14:00:38', '2022-12-12 14:00:38'),
(328, 'hinal', '5847837468', 'pune', 500000, NULL, 'eng', '2022-12-14 16:16:59', '2022-12-14 16:16:59'),
(330, 'pankhil', '934579383457', 'katol', 500002, NULL, 'dev', '2022-12-14 16:16:59', '2022-12-14 16:16:59'),
(333, 'sahil', '352746476', 'amritshar', 500001, NULL, 'eng', '2022-12-14 16:17:06', '2022-12-14 16:17:06'),
(336, 'Nidhi', '743678246', 'nagpur', 650000, NULL, 'HR', '2022-12-15 10:54:19', '2022-12-15 10:54:19'),
(337, 'hinal', '5847837468', 'pune', 500000, NULL, 'eng', '2022-12-19 16:37:35', '2022-12-19 16:37:35'),
(339, 'pankhil', '3564425473', 'Agra', 500002, NULL, 'dev', '2022-12-19 16:37:35', '2022-12-19 16:37:35'),
(342, 'hinal', '632874637', 'usa', 500000, NULL, 'eng', '2022-12-19 16:37:40', '2022-12-19 16:37:40'),
(348, 'sahil', '764873256', 'pune', 500001, NULL, 'eng', '2022-12-19 16:37:44', '2022-12-19 16:37:44'),
(352, 'Ranu', '7583768746', 'pune', 45000, NULL, 'HR', '2022-12-23 14:20:56', '2022-12-23 14:20:56'),
(353, 'Uday', '368726464', 'usa', 60000, NULL, 'Devloper', '2022-12-23 14:20:56', '2022-12-23 14:20:56'),
(354, 'Laukik', '274532712', 'agra', 50000, NULL, 'CEO', '2022-12-23 14:20:56', '2022-12-23 14:20:56'),
(355, 'Poshita', '67487236', 'mumbai', 23000, NULL, 'HR', '2022-12-23 14:20:56', '2022-12-23 14:20:56'),
(356, 'soumik', '63487365', 'nagpur', 90000, NULL, 'Devloper', '2022-12-23 14:20:56', '2022-12-23 14:20:56'),
(357, 'Ranu', '7583768746', 'pune', 45000, NULL, 'HR', '2022-12-23 14:21:18', '2022-12-23 14:21:18'),
(358, 'Uday', '368726464', 'usa', 60000, NULL, 'Devloper', '2022-12-23 14:21:18', '2022-12-23 14:21:18'),
(359, 'Laukik', '274532712', 'agra', 50000, NULL, 'CEO', '2022-12-23 14:21:18', '2022-12-23 14:21:18'),
(360, 'Poshita', '67487236', 'mumbai', 23000, NULL, 'HR', '2022-12-23 14:21:18', '2022-12-23 14:21:18'),
(361, 'soumik', '63487365', 'nagpur', 90000, NULL, 'Devloper', '2022-12-23 14:21:18', '2022-12-23 14:21:18'),
(362, 'Ranu', '7583768746', 'pune', 45000, NULL, 'HR', '2022-12-23 14:24:58', '2022-12-23 14:24:58'),
(363, 'Uday', '368726464', 'usa', 60000, NULL, 'Devloper', '2022-12-23 14:24:58', '2022-12-23 14:24:58'),
(364, 'Laukik', '274532712', 'agra', 50000, NULL, 'CEO', '2022-12-23 14:24:58', '2022-12-23 14:24:58'),
(365, 'Poshita', '67487236', 'mumbai', 23000, NULL, 'HR', '2022-12-23 14:24:58', '2022-12-23 14:24:58'),
(366, 'soumik', '63487365', 'nagpur', 90000, NULL, 'Devloper', '2022-12-23 14:24:58', '2022-12-23 14:24:58'),
(367, 'Ranu', '7583768746', 'pune', 45000, NULL, 'HR', '2022-12-23 14:29:35', '2022-12-23 14:29:35'),
(368, 'Uday', '368726464', 'usa', 60000, NULL, 'Devloper', '2022-12-23 14:29:35', '2022-12-23 14:29:35'),
(369, 'Laukik', '274532712', 'agra', 50000, NULL, 'CEO', '2022-12-23 14:29:35', '2022-12-23 14:29:35'),
(370, 'Poshita', '67487236', 'mumbai', 23000, NULL, 'HR', '2022-12-23 14:29:35', '2022-12-23 14:29:35'),
(371, 'soumik', '63487365', 'nagpur', 90000, NULL, 'Devloper', '2022-12-23 14:29:35', '2022-12-23 14:29:35'),
(373, 'Uday', '368726464', 'usa', 60000, NULL, 'Devloper', '2022-12-23 14:29:40', '2022-12-23 14:29:40'),
(374, 'Laukik', '274532712', 'agra', 50000, NULL, 'CEO', '2022-12-23 14:29:40', '2022-12-23 14:29:40'),
(375, 'Poshita', '67487236', 'mumbai', 23000, NULL, 'HR', '2022-12-23 14:29:40', '2022-12-23 14:29:40'),
(376, 'soumik', '63487365', 'nagpur', 90000, NULL, 'Devloper', '2022-12-23 14:29:40', '2022-12-23 14:29:40');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=377;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
